#web-bot-simples
#Web Bot que coleta dados do site da Dafiti

#Feito para estudo de web scrapling

#IMPORTANDO BIBLIOTECAS
from bs4 import BeautifulSoup
import requests

import time
import json
import os
wait = 10

dados_produtos = []
print('-'*30)
print('\t    Web-bot')
print('-'*30)
print('Site onde serão coletado os dados: Dafiti')
print()


for x in range(1,5):
	print('Coletando {}ª remessa de dados...'.format(x))

	#time.sleep(wait) #evita muitas requisições de uma vez no site

	url = "https://www.dafiti.com.br/roupas-masculinas/?page={}".format(x)
	site = requests.get(url)

	soup = BeautifulSoup(site.text, 'html.parser')

	produtos_boxs = soup.find_all('div', {'class': 'product-box'})
	# Coloca os dados dos produtos dentro da variável dados_produtos

	for produto in produtos_boxs:
		try:
			dados_produtos.append({
				"nome": produto.find('p', {'class':'product-box-title'}).contents[0],
				"preco": produto.find('span', {'class': 'product-box-price-from'}).contents[0]})
		except:
			print('\tRemovendo tag vazia...')

with open('dados.json', 'w') as f:
	json.dump(dados_produtos, f, indent = 4, sort_keys=True)


print("Dados gravados com sucesso!")

