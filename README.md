# DWB - Dafiti Web Bot
Web scraping feito em python para capturar dados do site Dafiti

# Requisitos para executar o programa:
- [Python 3.8.5](https://www.python.org/)
- [BeautifulSoup4](https://pypi.org/project/beautifulsoup4/) (biblioteca python)
- [Requests](https://pypi.org/project/requests/) (biblioteca python)
- Conexão com a Internet

> Para instalar as bibliotecas, execute o comando: `pip install -r requirements.txt` no seu terminal

Após a instalação das bibliotecas, execute `python bot.py` no seu terminal para começar a coleta de dados

Todos os dados coletados ficarão salvos num arquivo chamado "dados.json"

**AVISO**: Este script foi feito com propósitos de estudo. Não tenho nenhuma intenção com esses dados, apenas coletá-los e logo após excluí-los.
